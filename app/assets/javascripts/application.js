// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require jquery.flexslider-min
//= require angular
//= require angular-resource
//= require angular-local-storage
//= require custom
//= require ng-rails-csrf
//= require sweet-alert.js


(function() {
  var app =  angular.module('tijuanaGeek', ['LocalStorageModule', 'ngResource', 'ng-rails-csrf']);


  app.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('ls');
  }]);





  app.controller('principal', ['$scope', '$http', 'localStorageService', '$location', function($scope, $http, localStorageService, $location) {
    $scope.hostactual = $location.host();
    $scope.puertoactual = $location.port();


    $scope.saved = localStorage.getItem('productos');
    $scope.productos = (localStorage.getItem('productos')!==null) ? JSON.parse($scope.saved) : [];
    localStorage.setItem('productos', JSON.stringify($scope.productos));



    $scope.producto = {
      cantidad: 1,
      costo: 0,
      producto_id: '',
      tpmoneda: '',

      pnombre: '',

      imagen: '',
      uuid: Date.now(),
      totalproducto: 0,

      costototal: function(){

        return this.cantidad * this.costo;

      }
    };


    $scope.total = 0;
    $scope.costototal = 0;
    $scope.email = '';
    $scope.comentario = '';
    //$scope.userid = '';









    $scope.getProducto = function(pdid){
      $http.get('/principal/'+pdid+'/ver-producto.json').
          success(function(data, status, headers, config) {
            $scope.producto.cantidad = 1;
            $scope.producto.pnombre = data.nombre;
            $scope.producto.costo = data.costo;
            $scope.producto.producto_id = data.id;
            $scope.producto.imagen =data.url;
            $scope.producto.uuid= Date.now();
            $scope.producto.imagen = data.imagen['url'];
            $scope.producto.tpmoneda = data.tpmoneda;
            $scope.producto.totalproducto = data.costo;



              }
            );

    };





    $scope.$watch("cantidad", function() {

      $scope.totalproducto = $scope.cantidad*$scope.costo;
    });

    $scope.calcularTotal = function(){
      $scope.total = 0;
      angular.forEach($scope.productos, function(value, key)
      {

        $scope.total+=value.totalproducto;

      });



    };


    $scope.addproducto = function() {
      $scope.producto.totalproducto = $scope.producto.costo*$scope.producto.cantidad;
      $scope.copear = angular.copy($scope.producto);
      $scope.productos.push($scope.copear);
      localStorage.setItem('productos', JSON.stringify($scope.productos));



      $('#InviteTeacher').toggle(function(){
        $(this).val('Pending Request');
      }, function(){
        $(this).val('Invite');
      });

      swal({
        title: "Agregado",
        text: "El producto" + $scope.producto.nombre + " fue agregado\n con la cantidad de:"+$scope.producto.cantidad,
        type: "success",
        showCancelButton: false,
        confirmButtonClass: 'btn-success',
        confirmButtonText: 'Entendido!'
      });


    };

    $scope.oneclickadd = function(ide){
      $http.get('/principal/'+ide+'/ver-producto.json').
          success(function(data, status, headers, config) {
            $scope.productos.push(
                $scope.producto = {
                  cantidad: 1,
                  costo: data.costo,
                  producto_id: data.id,
                  tpmoneda: '',

                  pnombre: data.nombre,

                  imagen: data.imagen['url'],
                  uuid: Date.now(),
                  totalproducto: data.costo,

                  costototal: function(){

                    return this.cantidad * this.costo;

                  }
                }
            );
            localStorage.setItem('productos', JSON.stringify($scope.productos));
            swal({
              title: "Agregado",
              text: "El producto" + $scope.producto.nombre + " fue agregado\n con la cantidad de:"+$scope.producto.cantidad,
              type: "success",
              showCancelButton: false,
              confirmButtonClass: 'btn-success',
              confirmButtonText: 'Entendido!'
            });





          }
      )


    };

    $scope.limpiarProductos = function(){
      $scope.productos = [];
      localStorage.setItem('productos', JSON.stringify($scope.productos));


      localStorage.clear();
    };

    $scope.eliminarProducto = function(producto) {
      var index = $scope.productos.indexOf(producto);

      $scope.productos.splice(producto, 1);

      localStorage.setItem('productos', JSON.stringify($scope.productos));
      swal({
        title: "Porducto eliminada",
        text: "El producto fue eliminado",
        type: "success",
        showCancelButton: false,
        confirmButtonClass: 'btn-success',
        confirmButtonText: 'Entendido!'
      });

    };

    $scope.checkout = function(productos){

      var res = $http.post('/api/v1/pedidos',  {pedido: {total:$scope.total, status: "Solicitud de pedido", email: $scope.email, user_id: $scope.userid, comentario: $scope.comentario,  pedido_productos_attributes: productos }} );
      res.success(function(data, status, headers, config) {
        $scope.message = data;

        $scope.limpiarProductos();
        swal({
          title: "Pedido realizado",
          text: "El pedido se realizo correctamente recibira un E-mail de confirmacion",
          type: "success",
          showCancelButton: false,
          confirmButtonClass: 'btn-success',
          confirmButtonText: 'Entendido!'
        });
      });
      res.error(function(data, status, headers, config) {
        alert( "failure message: " + JSON.stringify({data: data}));
      });



    };








  }]);

  $(function () {
    $('.button-checkbox').each(function () {

      // Settings
      var $widget = $(this),
          $button = $widget.find('button'),
          $checkbox = $widget.find('input:checkbox'),
          color = $button.data('color'),
          settings = {
            on: {
              icon: 'glyphicon glyphicon-check'
            },
            off: {
              icon: 'glyphicon glyphicon-unchecked'
            }
          };

      // Event Handlers
      $button.on('click', function () {
        $checkbox.prop('checked', !$checkbox.is(':checked'));
        $checkbox.triggerHandler('change');
        updateDisplay();
      });
      $checkbox.on('change', function () {
        updateDisplay();
      });

      // Actions
      function updateDisplay() {
        var isChecked = $checkbox.is(':checked');

        // Set the button's state
        $button.data('state', (isChecked) ? "on" : "off");

        // Set the button's icon
        $button.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$button.data('state')].icon);

        // Update the button's color
        if (isChecked) {
          $button
              .removeClass('btn-default')
              .addClass('btn-' + color + ' active');
        }
        else {
          $button
              .removeClass('btn-' + color + ' active')
              .addClass('btn-default');
        }
      }

      // Initialization
      function init() {

        updateDisplay();

        // Inject the icon if applicable
        if ($button.find('.state-icon').length == 0) {
          $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
        }
      }
      init();
    });
  });

})();
