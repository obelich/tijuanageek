class Api::V1::PedidosController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:create]
  load_and_authorize_resource except: [:create]

  respond_to :json

  def index
    @pedidos = Pedido.all
    respond_with(@pedidos)
  end

  def show
    respond_with(@pedido)
  end

  def new
    @pedido = Pedido.new
    respond_with(@pedido)
  end

  def edit
  end

  def create

    @pedido = Pedido.new(pedido_params)
    if user_signed_in?
        @pedido.user_id = current_user.id
    end

    respond_to do |format|
          if @pedido.save
            format.html { redirect_to @pedido, notice: 'Datos guardados' }
            PedidoMailer.confirmarpedido(@pedido).deliver
            # format.json { render :show, status: :created, location: @pedido }
            format.json { render json: @pedido, status: 200}
          else
            format.html { render :new }
            format.json { render json: @pedido.errors, status: :unprocessable_entity }
          end
        end
  end

  def update
    @pedido.update(pedido_params)
    respond_with(@pedido)
  end

  def destroy
    @pedido.destroy
    respond_with(@pedido)
  end

  private
    def set_pedido
      @pedido = Pedido.find(params[:id])
    end

    def pedido_params
      # params.require(:pedido).permit(:producto_id, :cantidad, :user_id, :status, :costo, :status, :nombre, :total)
      params.require(:pedido).permit(:total, :status, :email, :user_id,
           pedido_productos_attributes: [:producto_id, :pedido_id, :cantidad, :costo, :totalproducto, :id, :pnombre])
    end
end
