class MenusController < ApplicationController
  before_action :set_menu, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :validarusuario
  layout 'admin'
  respond_to :html


  def validarusuario
    # raise StandardError, "#{current_user.roles.map(&:name).inspect}"
    if current_user.has_role? :Cliente
      authorize! :accion, User
    else

    end
  end


  def index
    @menus = Menu.all
    respond_with(@menus)
  end

  def show
    respond_with(@menu)
  end

  def new
    @menu = Menu.new
    @categorias = Categoria.all
    @submenus = Submenu.all
    respond_with(@menu)
  end

  def edit
    @submenus = Submenu.all
    @categorias = Categoria.all
  end

  def create
    # raise "hola"
    @menu = Menu.new(menu_params)
    @menu.save
    respond_with(@menu)
  end

  def update
    @menu.update(menu_params)
    respond_with(@menu)
  end

  def destroy
    @menu.destroy
    respond_with(@menu)
  end

  private
    def set_menu
      @menu = Menu.find(params[:id])
    end

    def menu_params
      params.require(:menu).permit(:nombremenu, :categoria_id, :descripcion, :imagenmenu, :menu_id, :posicion, :status, :primario,
      {:categoria_ids => []} )
    end
end
