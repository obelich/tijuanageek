class NoticiasController < ApplicationController
  before_action :set_noticia, only: [:show, :edit, :update, :destroy]

  def index
    @noticias = Noticia.all
    respond_with(@noticias)
  end

  def show
    respond_with(@noticia)
  end

  def new
    @noticia = Noticia.new
    respond_with(@noticia)
  end

  def edit
  end

  def create
    @noticia = Noticia.new(noticia_params)
    @noticia.save
    respond_with(@noticia)
  end

  def update
    @noticia.update(noticia_params)
    respond_with(@noticia)
  end

  def destroy
    @noticia.destroy
    respond_with(@noticia)
  end

  private
    def set_noticia
      @noticia = Noticia.find(params[:id])
    end

    def noticia_params
      params.require(:noticia).permit(:titulo, :noticia, :categoria_id)
    end
end
