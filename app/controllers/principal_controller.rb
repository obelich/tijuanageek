class PrincipalController < ApplicationController



  def index
    @productos = Producto.where(activo: true, portada: true)

  end

  def productos
    if params[:productoid]
      @productos = Producto.where(categoria_id: params[:productoid])
    elsif params[:menuid]
     @productos = Producto.joins(:menus).where("menus.id = ?", params[:menuid])
    elsif params[:submenuid]
      # raise "hola"
      @productos = Producto.joins(:submenus).where("submenus.id = ?", params[:submenuid])
    elsif params[:subsubmenuid]
      # raise "hola"
      @productos = Producto.joins(:subsubmenus).where("subsubmenus.id = ?", params[:subsubmenuid])
    end

  end

  def noticias
  end

  def eventos
  end

  def ubicacion
  end

  def contacto
  end

  def quienes_somos
  end

  def ver_producto

    @producto = Producto.find_by(id: params[:producto_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @producto }
    end

  end

  def checkout
    @productos = Producto.where(id: params[:productos].gsub(/[\[\]]/, '').split(","))
    # raise StandardError, "#{@productos.inspect}"
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @productos }
    end

  end

end
