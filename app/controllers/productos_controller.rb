class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]
  before_action :set_dropdownlist, only: [:new, :create, :edit, :update]
  before_action :authenticate_user!
  layout 'admin'
  before_action :validarusuario


  def validarusuario
    if current_user.roles.map(&:name) == ["Cliente"]
      authorize! :accion, User
    else

    end

  end


  def index
    if params[:producto_id]
      @productos = Producto.where(producto_id: params[:producto_id])
    else

      @productos = Producto.all
    end

    respond_with(@productos)
  end

  def show
    respond_with(@producto)
  end

  def new
    @producto = Producto.new
    respond_with(@producto)
  end

  def edit
  end

  def create
    @producto = Producto.new(producto_params)
    @producto.save
    respond_with(@producto)
  end

  def update
    # raise "hola"
    params[:producto][:menu_ids] ||= []
    params[:producto][:submenu_ids] ||= []
    params[:producto][:subsubmenu_ids] ||= []
    @producto.update(producto_params)
    respond_with(@producto)
  end

  def destroy
    @producto.destroy
    respond_with(@producto)
  end

  private

    def set_dropdownlist
      @categorias = Categoria.all
      @tipos = Tipo.all
      @marcas = Marca.all
    end
    def set_producto
      @producto = Producto.find(params[:id])
    end

    def producto_params
      params.require(:producto).permit(:nombre, :descripcion, :costo, :imagen, :categoria_id, :nuevo, :sleader, :sleadertext, :especial,
      :activo, :tpmoneda, :remove_imagen, :notas, :portada, :sku, :preciocosto, :preciosugerido, :porcentajeganacia, :costoproveedorenvio, :usarcosto,
      :marca_id, :tipo_id, {:menu_ids => []}, {:submenu_ids => []}, {:subsubmenu_ids => []})
    end
end
