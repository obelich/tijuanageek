class Menu < ActiveRecord::Base
  has_and_belongs_to_many :productos, join_table: :menus_productos
  has_many :submenus
  has_many   :productos, through: :menus_productos

  accepts_nested_attributes_for :submenus,  :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}

  mount_uploader :imagenmenu, ImagenmenuUploader
end
