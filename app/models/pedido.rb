class Pedido < ActiveRecord::Base
  # has_and_belongs_to_many :productos
  has_many :pedido_productos
  has_many :productos, through: :pedido_productos

  accepts_nested_attributes_for :pedido_productos
end
