json.array!(@noticias) do |noticia|
  json.extract! noticia, :id, :titulo, :noticia, :categoria_id
  json.url noticia_url(noticia, format: :json)
end
