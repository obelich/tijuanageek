json.array!(@pedidos) do |pedido|
  json.extract! pedido, :id, :producto_id, :cantidad, :user_id, :status
  json.url pedido_url(pedido, format: :json)
end
