class AddColumnPreciosToProductos < ActiveRecord::Migration
  def change
    add_column :productos, :preciocosto, :float
    add_column :productos, :preciosugerido, :float
    add_column :productos, :porcentajeganacia, :float
    add_column :productos, :costoproveedorenvio, :float
    add_column :productos, :usarcosto, :string
  end
end
