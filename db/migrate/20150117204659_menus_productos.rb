class MenusProductos < ActiveRecord::Migration
  def change
    create_table :menus_productos, :id => false do |t|
      t.references :menu
      t.references :producto
    end
    add_index :menus_productos, :menu_id
    add_index :menus_productos, :producto_id
  end
end
