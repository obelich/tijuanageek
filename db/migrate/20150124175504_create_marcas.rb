class CreateMarcas < ActiveRecord::Migration
  def change
    create_table :marcas do |t|
      t.string :marca
      t.text :descripcion

      t.timestamps
    end
    add_column :productos, :marca_id, :integer
    add_column :productos, :tipo_id, :integer
  end
end
