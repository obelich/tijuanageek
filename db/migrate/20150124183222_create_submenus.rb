class CreateSubmenus < ActiveRecord::Migration
  def change
    create_table :submenus do |t|
      t.string :submenu
      t.integer :menu_id
      t.text :descripcion

      t.timestamps
    end
  end
end
