class Menustipos < ActiveRecord::Migration
  def change
      create_table :menus_tipos, :id => false do |t|
        t.references :menu
        t.references :tipo
      end
      add_index :menus_tipos, :menu_id
      add_index :menus_tipos, :tipo_id
    end
end
