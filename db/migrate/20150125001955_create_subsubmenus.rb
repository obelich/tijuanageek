class CreateSubsubmenus < ActiveRecord::Migration
  def change
    create_table :subsubmenus do |t|
      t.string :submenu
      t.integer :submenu_id
      t.text :descripcion

      t.timestamps
    end
  end
end
