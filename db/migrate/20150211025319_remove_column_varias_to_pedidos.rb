class RemoveColumnVariasToPedidos < ActiveRecord::Migration
  def change
    remove_column :pedidos, :costo, :float
    remove_column :pedidos, :producto_id, :integer
    remove_column :pedidos, :cantidad, :integer
    remove_column :pedidos, :nombre, :string
  end
end
